var gulp = require('gulp');

var del = require('del');
var prefix = require('gulp-autoprefixer');
var changed = require('gulp-changed');
var concat = require('gulp-concat');
var csso = require('gulp-csso');
var less = require('gulp-less');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var rebaseUrl = require('gulp-css-rebase-urls');
var rjs = require('requirejs');

var config = {
    'autoprefixer-options': [
        'ie >= 9',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ],
    'production': false
};

var paths = {
  scripts: 'js/jquery.otcpharm-header.js',
  styles: 'less/otcpharm-header.less'
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use all packages available on npm
gulp.task('clean', function(cb) {
  // You can use multiple globbing patterns as you would with `gulp.src`
  del(['build'], cb);
});

gulp.task('scripts', ['clean'], function() {
  // Minify and copy all JavaScript (except vendor scripts)
  // with sourcemaps all the way down
  return gulp.src(paths.scripts)
      .pipe(uglify())
      .pipe(concat('jquery.otcpharm-header.min.js'))
      .pipe(gulp.dest('dist'));
});

gulp.task('styles', ['clean'], function() {
    return gulp.src(paths.styles)
        .pipe(less())
        .pipe(rebaseUrl({root: 'dist'}))
        .pipe(prefix(config['autoprefixer-options'], { cascade: true }))
        .pipe(concat('otcpharm-header.min.css'))
        .pipe(csso())
        .pipe(gulp.dest('dist'));
});

// Rerun the task when a file changes
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['scripts', 'styles']);
});


gulp.task('go', ['scripts', 'styles']);

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'scripts', 'styles']);