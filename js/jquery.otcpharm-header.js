/* ========================================================================
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    
    var VERSION = '1.0.0';

    var _this;
    
    // Create the defaults once
    var otcpharmHeader = 'otcpharmHeader',
        defaults = {

        };

    function Plugin( element, options ) {
        this.element = element;

        this.options = $.extend( {
        }, defaults, options);
        
        this._defaults = defaults;
        this._name = otcpharmHeader;
        
        this.version = VERSION;

        _this = this;

        this.initialize();
    }

    Plugin.prototype = {
        initialize: function () {
            $('body').on('click.otcpharm-header', '.js__otcpharm-header-menu-toggle', function(){
                $('.js__otcpharm-header-menu').toggleClass('open');
                $('.js__otcpharm-header-menu-toggle').toggleClass('open');
            });

            $(window).on('resize', function () {
                _this.nav();
            });

            this.navScroll();

            setTimeout(function(){
                _this.nav();
            }, 200);
        },

        nav: function () {
            var $container = $('.js__otcpharm-header-menu');
            var $wrap = $('.js__otcpharm-header-wrap');
            var $left = $('.js__otcpharm-header-scroll-left');
            var $right = $('.js__otcpharm-header-scroll-right');

            if($container.width() <= $wrap.width()){
                $wrap.css('margin-left', 0);
                $right.addClass('active');
                $left.removeClass('active');
            }else{
                $right.removeClass('active');
                $left.removeClass('active');
            }
        },

        navScroll: function () {
            var $container = $('.js__otcpharm-header-menu');
            var $wrap = $('.js__otcpharm-header-wrap');
            var $left = $('.js__otcpharm-header-scroll-left');
            var $right = $('.js__otcpharm-header-scroll-right');

            $right.off('mouseenter').on('mouseenter', function(){
                if($container.width() > $wrap.width()){
                    return false;
                }

                $wrap.css('margin-left', $container.width() - $wrap.width() - $left.width());

                $left.addClass('active');
                $right.removeClass('active');
            });

            $left.off('mouseenter').on('mouseenter', function(){
                $wrap.css('margin-left', 0);

                $right.addClass('active');
                $left.removeClass('active');
            });
        }
    };

    $.fn[otcpharmHeader] = function ( options ) {
        // global events

        return this.each(function () {
            if (!$.data(this, "plugin_" + otcpharmHeader)) {
                $.data(this, "plugin_" + otcpharmHeader,
                new Plugin( this, options ));
            }
        });
    };

    function dataInit(){
        if($('[data-otcpharm-header]').length > 0){
            $('[data-otcpharm-header]').otcpharmHeader();
        }
    }
    
    $(function() {
        dataInit();
    });

}));